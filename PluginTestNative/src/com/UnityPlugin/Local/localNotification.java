package com.UnityPlugin.Local;

import java.util.Calendar;

import com.unity3d.player.UnityPlayer;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class localNotification
{
	
	public void sendNotification(String message, String title_message,int unixtime, int primary_key,String log)
	{
		Log.i("Unity", "SendNotificationStart");
		
		//インテント作成
		Activity activity = UnityPlayer.currentActivity;
		Context context = activity.getApplicationContext();
		Intent intent = new Intent(context,NotificationReceiver.class);
		//戻す値
		intent.putExtra("MESSAGE", message);
		intent.putExtra("PRIMARY_KEY", primary_key);
		intent.putExtra("TITLE_MESSAGE",title_message);
		intent.putExtra("LOG",log);
		
		//アラーム
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.add(Calendar.SECOND, unixtime);//時間の単位,設定値
		
		PendingIntent sender = PendingIntent.getBroadcast(context, primary_key, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager alarm = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		alarm.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);
		
	}
	
	public void CancelLocalNotification()
	{
		Context context = UnityPlayer.currentActivity.getApplicationContext();
		
		Intent intent = new Intent(context,NotificationReceiver.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
		AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		am.cancel(pendingIntent);
		
	}
	
	
}
