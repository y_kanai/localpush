package com.UnityPlugin.Local;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


public class NotificationReceiver extends BroadcastReceiver
{
	
	public void onReceive(Context context,Intent intent)
	{
		
		String message = intent.getStringExtra("MESSAGE");
		String title_message = intent.getStringExtra("TITLE_MESSAGE");
		Integer primary_key = intent.getIntExtra("PRIMARY_KEY", 0);	
		
		
		//	localNotificationクラスからUnityPlayerNativeActivityクラスを読み込み Class<?> cc にモデル化
		Class<?> cc = null;
		try
		{
			cc = context.getClassLoader().loadClass("com.unity3d.player.UnityPlayerNativeActivity");
		}
		catch(ClassNotFoundException e1)
		{
			e1.printStackTrace();
			return;
		}
		
		intent = new Intent(context,cc);
		intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		
		//	新しく作ったintentからPendingIntentを作成
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
		
		// LargeIcon の Bitmap を生成
		final PackageManager pm = context.getPackageManager();
        ApplicationInfo applicationInfo = null;
        try {
          applicationInfo = pm.getApplicationInfo(context.getPackageName(),PackageManager.GET_META_DATA);
        } catch (NameNotFoundException e) {
          e.printStackTrace();
          return;
        }
        final int appIconResId = applicationInfo.icon;
        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), appIconResId);

        // NotificationBuilderを作成
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentIntent(pendingIntent);		//	通知が選択された時に起動するintentを設定
        builder.setTicker("ticker");  					//	ステータスバーに届くテキスト
        builder.setSmallIcon(appIconResId);          	//	アイコン
        builder.setContentTitle(title_message);      	// 	タイトル
        builder.setContentText(message);             	// 	本文（サブタイトル）
        builder.setLargeIcon(largeIcon);              	//	開いた時のアイコン
        builder.setWhen(System.currentTimeMillis());  	//	通知に表示される時間
        
        
        
        // 通知時の音・バイブ・ライト
        builder.setDefaults(Notification.DEFAULT_ALL);
        builder.setAutoCancel(true);

        // NotificationManagerを取得
        NotificationManager manager = (NotificationManager) context.getSystemService(Service.NOTIFICATION_SERVICE);
        // Notificationを作成して通知
        manager.notify(primary_key, builder.build());
        
      
	}
	
}
